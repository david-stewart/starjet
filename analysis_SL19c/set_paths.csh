#!/bin/csh
#STARJet
setenv STARLIB_VER   SL19c
setenv BASEPATH  /star/u/djs232/AuAuIncJet_gpc_code_review/vBitBucket/starjet
setenv BASEPATH_RUSNAK   /star/u/rusnak/JET_analysis_run11/STARJet
setenv STARJETBASEDIR $BASEPATH 
setenv ANALYSISDIR $BASEPATH/analysis_$STARLIB_VER
setenv LD_LIBRARY_PATH $LD_LIBRARY_PATH\:$ANALYSISDIR/lib

#other software
setenv ROOUNFOLD $BASEPATH_RUSNAK/software_$STARLIB_VER/RooUnfold/v-trunk-custom
setenv LD_LIBRARY_PATH $LD_LIBRARY_PATH\:$ROOUNFOLD

setenv FASTJETDIR $BASEPATH_RUSNAK/software_$STARLIB_VER/fastjet3
setenv PATH $PATH\:$FASTJETDIR/bin
setenv LD_LIBRARY_PATH $LD_LIBRARY_PATH\:$FASTJETDIR/lib

#Parametrized model
setenv TOYMODELDIR /star/u/djs232/AuAuIncJet_gpc_code_review/vBitBucket/toymodel
setenv LD_LIBRARY_PATH $LD_LIBRARY_PATH\:$TOYMODELDIR/Production
setenv LD_LIBRARY_PATH $LD_LIBRARY_PATH\:$TOYMODELDIR/Analysis
setenv LD_LIBRARY_PATH $LD_LIBRARY_PATH\:$TOYMODELDIR/Unfolding
