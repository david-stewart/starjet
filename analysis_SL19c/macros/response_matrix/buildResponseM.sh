#!/bin/bash
source ../set_paths.sh
echo "ADir: $ANALYSISDIR"
starver $STARLIB_VER

TRG="MB"
export RMTYPE="BG_sp" #deltapT distribution: BG_sp|inplane|outplane
export V2CORR=0 # correct delta pT for event plane bias
export CENTRAL=1

if [ $V2CORR -eq 1 ]; then
	SUFF="_v2"
else
	SUFF="_normal"
fi
if [ $CENTRAL -eq 1 ]; then
CENTSUFF="_central"
PTLEADCUTS="5" #6 7"
else
CENTSUFF="_peripheral"
PTLEADCUTS="2 3" #"4 5 6 7"
fi

export V2PATH="$ANALYSISDIR/macros/EP_corrections" #path to correction histograms
export PATH_TO_DELTA_PT_HISTOGRAMS="$ANALYSISDIR/out/$TRG/embedding${CENTSUFF}_main"

mkdir -p $PATH_TO_DELTA_PT_HISTOGRAMS/rmatrix$SUFF

for RPARAM in 0.2 #0.3 0.4 #0.5
do
	export RPARAM
for PTLEAD in `echo $PTLEADCUTS`
do
   export PTLEAD
	root4star -l buildResponseM.C -q -b
done
done
