#!/bin/bash
STARLIB_VER="SL19c"
BASEPATH_RUSNAK="/star/u/rusnak/JET_analysis_run11/STARJet"
BASEPATH="/star/u/djs232/AuAuIncJet_gpc_code_review/vBitBucket/starjet"
export STARJETBASEDIR="$BASEPATH" 
export ANALYSISDIR="$BASEPATH/analysis_$STARLIB_VER"
export LD_LIBRARY_PATH="$LD_LIBRARY_PATH:$ANALYSISDIR/lib"

export ROOUNFOLD="$BASEPATH_RUSNAK/software_$STARLIB_VER/RooUnfold/v-trunk-custom"
export LD_LIBRARY_PATH="$LD_LIBRARY_PATH:$ROOUNFOLD"

export FASTJETDIR="$BASEPATH_RUSNAK/software_$STARLIB_VER/fastjet3"
export PATH="$PATH:$FASTJETDIR/bin"
export LD_LIBRARY_PATH="$LD_LIBRARY_PATH:$FASTJETDIR/lib"

export TOYMODELDIR="/star/u/djs232/AuAuIncJet_gpc_code_review/vBitBucket/toymodel"
export LD_LIBRARY_PATH="$LD_LIBRARY_PATH:$TOYMODELDIR/Production"
export LD_LIBRARY_PATH="$LD_LIBRARY_PATH:$TOYMODELDIR/Analysis"
export LD_LIBRARY_PATH="$LD_LIBRARY_PATH:$TOYMODELDIR/Unfolding"
