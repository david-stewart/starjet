#!/opt/star/sl73_gcc485/bin/python

'''
Some generic python library notes:
os.environ['keyword'] : check for the given environment varible

os.access( <directorypath>, os.W_OK ) and 
os.access( <directorypath>, os.X_OK ) give whether the directory is write and execute ok

subprocess.call(['sh','script']) to call a submission script.
'''

import stat
import os
import subprocess
from shutil import copyfile
from glob import glob


class CodeQA:
    '''
    A class used for the code QA of the Au+Au Inclusive Jet paper
    in Python 2.7.14 (the default RCAS version)


    History:
    There is a nice code How To code QA manual located at:
    https://drupal.star.bnl.gov/STAR/blog/djs232/star-200-gev-auau-inclusive-charged-jet-paper-code-qa

    Much of the original code was written by Alex Schmah for the 
    semi-inclusive h+jet paper for Au+Au 200 GeV collisions. The current
    code author is Jan Rusnak.

    The code is taken from Github (and will be frozen on STAR's CVS system 
    this week -- 2020.03.31).

    Running the code consists mostly of running a chain of scripts that
    communicate between the code pieces with environment variables. The point
    of this class is to check for the proper inputs and output at each stage
    of the process, so that the user can successfully run the code.


    Keyword input:
    argv[1] : name of code step to run
    '''

    # Note: this module is not a bad idea. However, it would require writing a syntax tree 
    # for getting the internal bash name-expansion assignment that won't work.
    #
    # def get_export_var(self, f_name, v_name):
    #     '''
    # 
    #     read through file f_name and find the export variable value v_name

    #     input:
    #         file-name
    #         variable-name

    #     output:
    #         variable-name export value (from bash file syntax like: export OUT_PATH="$ANALYSISDIR/out_test"

    #     exists program on error if it fails to fin
    #     '''

    #     try:
    #         with open(f_name,'r') as fin:
    #             for L in fin.readlines():
    #                 A = L.split()
    #                 if A[0] == 'export':
    #                     val = A[1].split('=')[1]
    #                     if val[0] == '"' and val[-1] == '"':
    #                         return val[1:-1]
    #                     else:
    #                         return val
    #         print('fatal: failed to find variable "%s" in file "%s"'%(v_name, f_name))
    #         exit(2)
    #     except:
    #         print('fatal: failed to find variable "%s" in file "%s"'%(v_name, f_name))
    #         exit(2)
    def backup_file(self, name):
        '''
        Back up file "name" to first unique name "name_bkup_%i"
        Returns the bkup name
        '''
        self.check_path(name,(1,1,0))
        i = 0
        while os.path.exists("%s_bkup_%i"%(name,i)):
            i+=1
        bkup = "%s_bkup_%i"%(name,i)
        copyfile(name, bkup)
        return bkup

    def mod_bash_input(self, name, bkup, bash_keys):
        '''
        Reads file bkup into name, but with the user provided input for keywords

        input:
            name: name of file to generate
            bkup: the name of the existing bash file that will be copied with the
                  updated input into file "name"
            bash_keys: the bash keys whose values will be udpated (note that python
                       will ask for live user input for which values to use)
        output:
            generate file named inp:name from inp:bkup
        '''

        #require the backup file
        self.check_path(bkup,(1,1,0))

        # get the raw input
        vals = dict()
        for key, description in bash_keys.items():
            vals[key] = raw_input("Enter values for %s (%s), default(%s): "%(key,description[0],description[1]))
            if not vals[key]:
                vals[key] = description[1]

        # modify the input
        # assume that all keys will be used once. Error otherwise
        # print(name)
        # print(bkup)
        with open(name,'w') as f_out:
            for L in open(bkup,'r').readlines():
                for key in vals.keys():
                    if '%s='%key in L:
                        print(L.strip())
                        val = vals.pop(key)
                        print('replacing value for %s'%key) 
                        print('<-- old line: "%s"'%L.strip())
                        A = L.split('%s='%key)
                        old_val = A[1].split()[0]
                        # print('old_val: %s'%old_val)
                        # print('val: %s'%val)
                        if '#' in old_val:
                            old_val = old_val.split('#')[0]
                        L=L.replace(old_val, val)
                        print('--> new line: "%s"'%L.strip())
                f_out.write(L)
     

    def check_path(self, path, req, term_on_failure=True, fail_msg=''):
        '''
        check if the path exists

        req = tuple of (read access, write access, executable access)

        if term_on_failure, terminate Python script if one of the req is not met
        '''

        is_good = False

        if not os.path.exists(path):
            if fail_msg:
                print(fail_msg)
            print('fatal: path not found:')
            print(path)
            if (term_on_failure):
                exit(2)
            else:
                return False


        if req[0] == 1 and not os.access(path, os.R_OK):
            if fail_msg:
                print(fail_msg)
            print('fatal: no read access for path:')
            print(path)
            if (term_on_failure):
                exit(2)
            else:
                return False

        if req[1] == 1 and not os.access(path, os.W_OK):
            if fail_msg:
                print(fail_msg)
            print('fatal: no write access for path:')
            print(path)
            if (term_on_failure):
                exit(2)
            else:
                return False

        if req[2] == 1 and not os.access(path, os.X_OK):
            if fail_msg:
                print(fail_msg)
            print('fatal: no exe. access for path:')
            print(path)
            if (term_on_failure):
                exit(2)
            else:
                return False

        return True

    def check_set_paths(self):
        '''
        Check that the appropriate environment variables from set_paths.sh
        are set.
        '''

        if self.verbose: 
            print('check:  Are set_paths.sh environment variables are set?')
        # directories with required write access
        req_W_OK = ['STARJETBASEDIR','ANALYSISDIR','TOYMODELDIR']
        # directories with required execute access
        req_X_OK = ['FASTJETDIR','ROOUNFOLD','FASTJETDIR']

        for D in req_W_OK:
            if not D in os.environ:
                print('fatal error: %s is not a set environment variable'%D)
                print('required: run the set_paths.sh scripts')
                exit(2)
            self.check_path(os.environ[D],(1,1,1),True)
            # path = os.environ[D]
            # if not os.access(path,os.W_OK):
                # print('fatal error: write access is required, but not granted for %s'%path)
                # exit(2)
            # if not os.access(path,os.X_OK):
                # print('fatal error: exe access is required, but not granted for %s'%path)
                # exit(2)

        for D in req_X_OK:
            if not D in os.environ:
                print('fatal error: %s is not a set environment variable'%D)
                print('required: run the set_paths.sh scripts')
                exit(2)
            self.check_path(os.environ[D],(1,0,1),True)
            # path = os.environ[D]
            # if not os.access(path,os.X_OK):
                # print('fatal error: write access is required, but not granted for %s'%path)
                # exit(2)

        if self.verbose: 
            print('result: Set_paths.sh environment variables are ok')

        #Check for the primary set_path.sh file
        # set_paths_script =   './starjet/analysis_SL19c/set_paths.sh'
        # if not os.path.isfile(set_paths_script):
            # exit('fatal error: %s note present'%set_paths_script


    def __init__(self, verbose=False):
        self.verbose = verbose
        '''
        Check to see if the paths have been initialied.
        '''

        # check that the ./set_paths.sh script has run well
        self.check_set_paths()

        # check the version of the STAR library
        if self.verbose: 
            print('check:  Is STAR_VERSION == SL19c?')
        try:
            if not os.environ['STAR_VERSION'] == "SL19c":
                print ("fatal: STAR library version not SL19c")
                print ("run command: starver SL19c")
                exit(2)
        except:
            exit(2)
        if self.verbose: 
            print('result: STAR_VERSION == SL19c')

        # check that the main code is compiled
        if not os.path.isdir("%s/.sl73_gcc485" % os.environ['ANALYSISDIR']):
            print ('code not compiled under path')
            print (os.environ['ANALYSISDIR'])
            inp = raw_input("Do you wish to run cons in %s?\n y/n: "%os.environ['ANALYSISDIR'] )
            if inp.lower() == 'y':
                os.chdir ( os.environ['ANALYSISDIR'] )
                print('------------')
                print('running cons')
                print('------------')
                subprocess.call(['cons'])
                print('-----------------')
                print('done running cons')
                print('-----------------')
            else:
                print('ok. goodbye!')

        # check for the local symlink to lib
        if not os.path.isdir("%s/lib"%os.environ['ANALYSISDIR']):
            os.chdir(os.environ['ANALYSISDIR'])
            os.symlink(".sl73_gcc485/lib","./lib")


        # check for output directory
        path = "%s/out" % os.environ['ANALYSISDIR']
        if not os.path.isdir(path):
            print("fatal: must have directory (or symlink) to $ANALYSISDIR/out")
            exit(2)
        if not (os.access(path, os.X_OK) and os.access(path, os.W_OK)):
            print("fatal: must have read and write access to $ANALYSISDIR/out")
            exit(2)

        
        # check that the analysis output directory is present

    def run_analysis_sample(self, exe=False):
        '''
        Used to check and run the $ANALYSISDIR/run_analysis_sample.sh

        Two main checks:
        check output directory location
        check that the input list exists and is the correct one (with "pico" in the list name")
        '''

        # check for existence of executable
        ex_file = '%s/run_analysis_sample.sh' % os.environ['ANALYSISDIR']
        self.check_path(ex_file, (1,0,1))

        # get the output directory location
        out_path = "%s/out_test" % os.environ['ANALYSISDIR']
        self.check_path(out_path, (1,1,1))

        # check for the existance and usefulless of the input file list
        input_list = "%s/filelists/test.list" % os.environ['STARJETBASEDIR']
        self.check_path(input_list, (1,1,0))

        # check if input list is the correct input list 
        for L in open(input_list):
            if not 'SL19c' in L:
                print('fatal: input file in input list "%s" does have "SL19c" in it'%input_list)
                print('line:')
                print(L)
                print('Update input file list: %s'%input_list)

                inp = raw_input("Do you wish to copy over suggested list: /star/u/rusnak/JET_analysis_run11/STARJet/filelists/test_STARpico.list?\n y/n: ")
                if inp.lower()[0]=='y':
                    i = 0
                    while os.path.isfile("%s__bkup_%i"%(input_list, i)):
                        i += 1
                    os.rename(input_list, "%s__bkup_%i"%(input_list, i))
                    copyfile('/star/u/rusnak/JET_analysis_run11/STARJet/filelists/test_STARpico.list', input_list)
                    break
                else:
                    exit(2)

        print ("%-7s: %s"%('exec', ex_file))
        print ("%-7s: %s"%('input', input_list))
        print ("%-7s: %s"%('output', out_path))
        if exe:
            os.chdir(os.environ['ANALYSISDIR'])
            subprocess.call(['sh','run_analysis_sample.sh'])
        
    def submit_analysis(self, exe=False, shorten_file_list=False):
        '''
        Used to check and run the $ANALYSISDIR/submitter/1-run_embedding_and_analysis/submit_analysis.sh

        Two main checks:
        output directory and location has already been checked (with the initialization of the class)

        generated input: 
            local tmp directory (make it if it doesn't exist)
            log and err directories at $ANALYSISDIR/submitter

        input filelists: 
            $STARJETBASEDIR/filelists/run11_AuAu200_STARpicoDst_191011.list
            $STARJETBASEDIR/filelists/run11_AuAu200_STARpicoDst_4embedding.list
         
        output:
            $ANALYSISDIR/out/<trigger -- generated>
        '''

        ex_dir  = '%s/submitter/1-run_embedding_and_analysis'%os.environ['ANALYSISDIR']
        ex_file = 'submit_analysis.sh'

        print('Looking to run %s/%s'%(ex_dir,ex_file))

        log_dir  = '%s/submitter/log'%os.environ['ANALYSISDIR']
        err_dir =  '%s/submitter/err'%os.environ['ANALYSISDIR']
    
        # check for ex file
        self.check_path(ex_dir, (1,1,1))
        self.check_path('%s/%s'%(ex_dir,ex_file), (1,0,1))

        # check for log_dir and err_dir
        if not self.check_path(log_dir,(1,1,1),False):
            print ('fatal: Output directory required for log files at:')
            print (log_dir)
            print ('Make a directory or symbolic link at this point.')
            exit (2)
        if not self.check_path(err_dir,(1,1,1),False):
            print ('fatal: Output directory required for err files at:')
            print (log_dir)
            print ('Make a directory or symbolic link at this point.')
            exit (2)

        # ensure the input file lists
        in_list_0 =   'run11_AuAu200_STARpicoDst_QAsample.list'
        in_list_1 =   'run11_AuAu200_STARpicoDst_4embedding_QAsample.list'
        in_list_path = '%s/filelists' % os.environ['STARJETBASEDIR']

        for in_list in (in_list_0, in_list_1):
            if not os.path.exists('%s/%s'%(in_list_path,in_list)):
                print ('warning: missing input file:')
                print ('%s/%s'%(in_list_path,in_list))

                list_rusnak = '%s/filelists/%s'%(os.environ['BASEPATH_RUSNAK'],in_list)
                if os.path.exists(list_rusnak):
                    print ('Would you like to copy the input list from: ')
                    print (list_rusnak)
                    user = raw_input (' y/n: ')
                    if user.lower()[0] == 'y':
                        copyfile(list_rusnak, '%s/%s'%(in_list_path,in_list))
                else:
                    print ('fatal: cannot find a backup input copy for missing list')
                    exit(2)

        # shorten the file-list, if desired
        if shorten_file_list:
            n_files = raw_input('how many files do you want in the input file list? ')
            n_files = int(n_files)
            for in_list in (in_list_0, in_list_1):
                # make a backup of the list
                i = 0
                while os.path.exists('%s/%s_bkup_%i'%(in_list_path,in_list,i)):
                    i += 1
                name = '%s/%s'        %(in_list_path,in_list)
                name_bkup = '%s/%s_bkup_%i'%(in_list_path,in_list,i)
                os.rename(name, name_bkup)
                n_lines = len(open(name_bkup,'r').readlines())
                if n_files < n_lines:
                    n_space = n_lines / n_files 

                i = 0
                n_files_added = 0
                with open(name,'w') as f_out:
                    for L in open(name_bkup,'r').readlines():
                        i += 1
                        if i == n_space:
                            i = 0
                        if  i == 0:
                            f_out.write(L)
                            n_files_added += 1
                            if n_files_added == n_files:
                                break



        # edit the input input file for DOEMBEDDING={0,1} and for number of events
        # n_events = raw_input('How many events per job should be run? (default: 10000000): ')
        # if not raw_input:
            # raw_input = "10000000"
        # n_events = int(n_events)

        # edit the do-embedding value
        # print ('select DOEMBEDDING value : 0 for real data, 1 for bkgd response matrix')
        # do_emb_option = raw_input (' selection value {0,1}: ')
        # do_emb_option = int(do_emb_option)

        # backup the input script and write it with the above options
        os.chdir(ex_dir)
        bkup = self.backup_file(ex_file)

        self.mod_bash_input(ex_file, bkup, 
            {
                'DOEMBEDDING':('0 or 1 for do or don\'t','0'),
                'NEVENTS':('number of events run','10000000')
            } )

        # copyfile( ex_file, '%s_bkup'%ex_file)

#         with open(ex_file,'w') as f_out:
#             for L in open('%s_bkup'%ex_file,'r').readlines():
#                 A = L.split()
#                 try:
#                     if A[0] == 'export' and not len(A) == 1:
#                         V = A[1].split('=')
#                         if len(V)==2:
#                             if V[0] == 'DOEMBEDDING':
#                                 f_out.write('export DOEMBEDDING=%i '%do_emb_option)
#                                 if len(A) > 2:
#                                     print(A)
#                                     f_out.write(' '.join(A[2:]))
#                                     f_out.write('\n')
#                                 else:
#                                     f_out.write('\n')
#                                 continue
#                             elif V[0] == 'NEVENTS':
#                                 f_out.write('export NEVENTS=%s '%n_events)
#                                 if len(A) > 2:
#                                     f_out.write(' '.join(A[2:]))
#                                     f_out.write('\n')
#                                 else:
#                                     f_out.write('\n')
#                                 continue
#                             else:
#                                 f_out.write(L)
#                         else:
#                             f_out.write(L)
#                     else:
#                         f_out.write(L)
#                 except:
#                     f_out.write(L)

        # print('input ex path:')
        # print('  %s'%ex_dir)
        # print('exe file:')
        # print('  %s'%ex_file)

        # if (do_emb_option == 0):
        #     print ("%-7s: %s/%s"%('input', in_list_path, in_list_0))
        # else:
        #     print ("%-7s: %s/%s"%('input', in_list_path, in_list_1))
        # print ("%-7s: %s"%('output', '%s/out/<trigger-name>'%(os.environ['ANALYSISDIR'])))

        if exe: 
            os.chdir(ex_dir)
            subprocess.call(['sh',ex_file])

    def buildResponseM_macro(self, exe=False):
        '''
        submit the interactive macro:
            ANALYSISDIR/macros/response_matrix/
            sh buildResponseM.sh

        input:
        /star/u/djs232/AuAuIncJet_gpc_code_review/v3/starjet/analysis_SL19c/macros/EP_corrections/v2corr.root (present)
        "path" = $ANALYSISDIR/out/MB/embedding_{central,peripheral}_main
        from which it reads:
        $ANALYSISDIR/out/MB/embedding_{central,peripheral}_main/histos_embeddedjet.root

        output:
        $ANALYSISDIR/out/MB/embedding_{central,peripheral}_main/rmatrix{_v2,_normal}

        '''

        input_0 = '%s/out/MB/embedding_central_main/histos_embeddedjet.root'% os.environ['ANALYSISDIR']
        input_1 = '%s/out/MB/embedding_peripheral_main/histos_embeddedjet.root'% os.environ['ANALYSISDIR']
        input_ex = '%s/macros/response_matrix/buildResponseM.C' % os.environ['ANALYSISDIR']
        
        output = '%s/out/MB/embedding_{central,peripheral}_main/rmatric{_v2,_normal}'

        # check for the input values:
        self.check_path(input_0,(1,1,0))
        self.check_path(input_1,(1,1,0))
        self.check_path(input_ex,(1,1,0))

        print('---------')
        print('Looking at running $ANALYSISDIR/macros/response_matrix/buildResponseM.sh macro directly')
        print('input: %s'%input_0)
        print('input: %s'%input_1)
        print('exe:   %s'%input_ex)
        print('output: %s'%output)
        print('---------')

        if (exe):
            os.chdir('%s/macros/response_matrix'%os.environ['ANALYSISDIR'])
            subprocess.call(['sh','buildResponseM.sh'])
    def buildResponseM(self, exe=False):
        '''
        Same as buildResponseM_macro, but for all the response matrices, by using:
        cd $ANALYSISDIR/submitter/2-create_response_matrices/
        sh submit_buildRM.sh

        So, same input, but the submittion script listed above
        '''
        ex_path = '%s/submitter/2-create_response_matrices'%os.environ['ANALYSISDIR']
        ex_script = 'submit_buildRM.sh'
        self.check_path('%s/%s'%(ex_path,ex_script),(1,1,0))

        print('---------')
        print('Looking at running $ANALYSISDIR/submitter/2-create_response_matrices/submit_buildRM.sh')
        print('This is a driver for the submission macro checked here')
        self.buildResponseM_macro(False)

        if exe:
            os.chdir(ex_path)
            subprocess.call(['sh',ex_script])
    def submit_pythia_embedding(self, exe=False):
        '''
        Check and run the code for:
        cd $TOYMODELDIR/submitter/DetectorFastSim
        sh submit_pythia_embedding.sh

        inputs:
            generated input:
            LOGDIR="$TOYMODELDIR/submitter/log"
            ERRDIR="$TOYMODELDIR/submitter/err"
            MACRODIR="$TOYMODELDIR/macros/DetectorFastSim"
            MACROFILE="run_pythiaEmb.C"
            OUT_PATH_FINAL="$TOYMODELDIR/DataOut/pythia/jetonly/$TYPE"
            library: $TOYMODELDIR/Production/libThrm.so
        
        input parameters (in multiply_matrix.sh) (make smaller)
            kEVENTS=2000 #how many thousands of events do we want to run
            export NEVENTS=200000 #number of events per job
        '''

        # setup and check the required input
        script_path  = "%s/submitter/DetectorFastSim"%os.environ['TOYMODELDIR']
        script_file = "submit_pythia_embedding.sh"

        LOGDIR="%s/submitter/log" % os.environ['TOYMODELDIR']
        ERRDIR="%s/submitter/err" % os.environ['TOYMODELDIR']
        MACRO="%s/macros/DetectorFastSim/run_pythiaEmb.C" % os.environ['TOYMODELDIR']
        OUT_PATH_FINAL="%s/DataOut" % os.environ['TOYMODELDIR']
        library =  "%s/Production/libThrm.so" % os.environ['TOYMODELDIR']

        for path in (LOGDIR, ERRDIR, MACRO, library, OUT_PATH_FINAL, script_path):
            self.check_path(path,(1,1,1))
        self.check_path('%s/%s'%(script_path,script_file),(1,1,0))

        print('-----')
        print('looking at running $TOYMODELDIR/macros/DetectorFastSim/run_pythiaEmb.C')
        for path in (LOGDIR, ERRDIR, MACRO, library, '%s/%s'%(script_path,script_file)):
            print('input path:  %s'%path)
        print('output path: $TOYMODELDIR/DataOut/pythia/jetonly/$TYPE')
        print('-----')
        if exe:
            # write backup and modify kEVENTS and NEVENTS
            os.chdir(script_path)
            bkup_name = self.backup_file(script_file)

            #modify the inputs for shorter running time
            self.mod_bash_input(script_file, bkup_name, 
                { 'kEVENTS' : ('how many thousand events (orig: 2000)','2000'),
                  'NEVENTS' : ('nEvents per job (orig:200000)','200000')
                }
            )
            os.chdir(script_path)
            subprocess.call(script_file)

    def hadd_and_link_toymodel(self, exe=False):
        '''
        hadd and link the toy model. Bodies for default scripts (if not present)
        are written from this script
        '''
        path = '%s/DataOut/pythia/jetonly'%os.environ['TOYMODELDIR']
        hadd_script = 'hadd.sh'
        ln_script   = 'make_emb_links_manually.sh'

        hadd_path = '%s/%s'%(path,hadd_script)
        ln_path   = '%s/%s'%(path,ln_script)

        self.check_path(path,[1,1,1])

        if not os.path.exists(hadd_path):
            print ('%s not present'%hadd_path)
            make_input = raw_input("generate default hadd.sh script? y/n: ")
            if make_input[0].lower() == 'y':
                with open (hadd_path,'w') as f_out:
                    f_out.write('''#!/bin/bash
for CENTRAL in "central" "peripheral"
do
	for RPARAM in 0.2 0.3 0.4
	do
            file=pythia_emb_R${RPARAM}.root
            dir=pyEmb_20k_charged_R${RPARAM}_effcorr_pTsmear_${CENTRAL}_2u1g_eff0_AuAu_trcuts1_momRes2_GPC_QA
            if [ -a $dir ]; then
                cd ${dir}
                if [ ! -f ./${file} ]; then
                    hadd ${file} *.root
                fi
                cd ..
            fi
	done
done
''')
                os.chmod(hadd_path, stat.S_IRWXU)

        if not os.path.exists(ln_path):
            print ('%s not present'%ln_path)
            make_input = raw_input("generate default make_emb_.sh script? y/n: ")
            if make_input[0].lower() == 'y':
                with open (ln_path,'w') as f_out:
                    f_out.write('''#!/bin/bash
for CENTRAL in "central" "peripheral"
do
	for RPARAM in 0.2 0.3 0.4
	do
            file=pyEmb_R${RPARAM}_${CENTRAL}_normal
    	    if [ -a ${file} ]; then 
                rm ${file}
            fi
            ln -s pyEmb_20k_charged_R${RPARAM}_effcorr_pTsmear_${CENTRAL}_2u1g_eff0_AuAu_trcuts1_momRes2_GPC_QA ${file}
	done
done
''')
                os.chmod(ln_path, stat.S_IRWXU)

        if exe:
            self.check_path(hadd_path,[1,1,1])
            self.check_path(ln_path,[1,1,1])
            os.chdir(path)
            subprocess.call(['sh',hadd_script])
            subprocess.call(['sh',ln_script])

    def multiply_matrix(self,exe=False):
        print(
        ''' Note: the input script $ANALYSISDIR/macros/response_matrix/multiply_matrix.sh
was edited by hand so that it would work, and then run by hand (and not by Python).
Therefore, this step is present as a reminder that it  needs to be done. However,
it (self.multiply_matrix()) hasn't been tested with Python''')
        path   = '%s/macros/response_matrix'%os.environ['ANALYSISDIR']
        script = 'multiply_matrix.sh'
        script_path = '%s/%s'%(path,script)
        self.check_path(script_path,[1,0,0])
        os.chdir(path)
        if exe:
            subprocess.call(['sh',script])

    def buildResponseROO(self,exe=False):
        '''Check input and output for:
        $ANALYSISDIR/2-create_response_matrices/submit_buildRMROO.sh
        and, if exe==True, run the script

        values to update in the scripts:
        TSUFFIX_ARR = "_normal"
        '''
        
        print('''
NB: This is the Python function CodeQA.buildResponseROO() which is to run the script
 $ANALYSISDIR/2-create_response_matrices/submit_buildRMROO.sh
 The internal BASH logic of that script is such that this Python function *does not*
 check all of the required inputs (they are set in loops).

 In the QA, the loops were modified from the archived version to have TSUFFIX_ARR 
 loop over only "_normal".

 Only the fixed required paths are checked by this script.
'''

        path   = '%s/submitter/2-create_response_matrices'%os.environ['ANALYSISDIR']
        script = 'submit_buildRMROO.sh'
        path_script = '%s/%s'%(path,script)

        # inputs = [
        logdir =  '%s/submitter/log'% os.environ['ANALYSISDIR'] 
        errdir =  '%s/submitter/err'% os.environ['ANALYSISDIR'] 
        workdir = '%s/macros/response_matrix'% os.environ['ANALYSISDIR']

        # update the number of events to 1% of those otherwise used
        self.check_path(logdir,[1,1,1])
        self.check_path(errdir,[1,1,1])
        self.check_path(workdir,[1,1,1])
        self.check_path(path_script,[1,1,1])

        # update the number of events


        script_path = '%s/%s'%(path,script)
        self.check_path(script_path,[1,0,0])
        os.chdir(path)
        if exe:
            subprocess.call(['sh',script])
        



if __name__ == '__main__':
    qa = CodeQA(verbose=False)

    #----------------------------------------------------------
    # Run the modules one by one (using comment and uncomment)
    # 
    # This is because while helpful, this script isn't fool-proof;
    # it should be run slowly, one module at a time.
    #----------------------------------------------------------

    #----------
    # prior bit
    #----------
    if False:
        qa.run_analysis_sample(False) # argument for it it should actually run the executable
        qa.run_analysis_sample(True) # argument for it it should actually run the executable
        qa.submit_analysis(False, False) # 

        qa.submit_analysis(True, False) # I used this to shorten the input files to 1k input files
        qa.buildResponseM_macro(True) # ok -- this runs *takes order 13 minutes to run* interactively 
        qa.buildResponseM(True) # done.
        qa.submit_pythia_embedding(True)

        # hadd together the embedding ROOT files and then make the soft links
        qa.hadd_and_link_toymodel(True)

    #-------------
    # current bit:
    #-------------
    qa.buildResponseROO(False)

    #------------
    # yet to come
    #------------
    if False:
        qa.multiply_response_matrix() # run cd $ANALYSISDIR/macros/response_matrix/; sh multiply_matrix.sh
