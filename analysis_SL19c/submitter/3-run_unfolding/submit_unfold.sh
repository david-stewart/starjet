#!/bin/bash
source set_paths.sh
SCRIPT_NAME=`basename -- $0`
_usage() {
    echo "Usage: ${SCRIPT_NAME} RMATRIX_TYPE"
    exit 1
}

export RMATRIX_TYPE=$1 #BG_sp BG_pyt dete BGD - correction for BG (using single particle / pythia jet), detector effects, BG+detector effects 
echo "RMATRIX_TYPR: $RMATRIX_TYPE"
#check arguments
[ -n "$RMATRIX_TYPE" ] || _usage

prior_type=(flat flat pythia powlaw4 powlaw45 powlaw5 powlaw55 tsalis_1 tsalis_2 tsalis_3 tsalis_4 tsalis_5 tsalis_6 tsalis_7 tsalis_8 tsalis_9)

BASEDIR="$ANALYSISDIR"
LOGDIR="$BASEDIR/submitter/log"
ERRDIR="$BASEDIR/submitter/err"
MACRODIR="$ANALYSISDIR/macros/unfolding"
MACRONAME="run_unfolding.C"
#MACRONAME="run_unfold_allpriors.csh"
	#MACRONAME="run_unfold_allpTlead.sh"
export SMOOTH=0 #smooth unfolded spectrum between iterations, for Bayes only
#export NBINS=100
export NBINS=VAR
export NITER=10 #number of iterations (bayesian unf.) or maximal value of the regularization parameter (SVD)
# FOR PRIOR DISTRIBUTION
export PTCUT=0.2
export SECONDUNFOLD=0 #unfold already unfolded results (eg. using a different RM)
   export INPUTITER=4 #if unfolding already unfolded results, which iteration to unfold
export EFFICORR=1 # do efficiency correction
#export SUF="_Xcheck-newRooUnf" #output dir suffix
export SUF="_GPC2" #output dir suffix

#export PRIORS="2 4 5 6 7 8 9 10 11 12 13 14 15"
export PRIOR_MIN=2 #start with this prior function
export PRIOR_MAX=15 #finish with this prior function
export PRIOR_SKIP1=3 #skip this prior function
export PRIOR_SKIP2=3
export PRIOR_SKIP3=3


for SVD in 1 0 # 1:SVD unfolding | 0:Bayesian unfolding
do
export SVD 
if [ $SVD -eq 1 ]; then
	UNFTYPE="SVD"
	#NITER=6 #maximal value of the regularization parameter
else
	UNFTYPE="Bayes"
fi


for CENTRAL in 0 1 #0 #0: peripheral Au+Au | 1: central Au+Au | 2: p+p
do
export CENTRAL
TRG="MB" #HT, MB
USE2DHISTO=1
if [ $CENTRAL -eq 1 ]; then
	SUFFIX="_central"
	#PTLEADCUTS="5 6 7"
	PTLEAD_MIN=5
	PTLEAD_MAX=7
elif [ $CENTRAL -eq 0 ]; then
	SUFFIX="_peripheral" 
	#PTLEADCUTS="4 5 6 7"
	PTLEAD_MIN=3
	PTLEAD_MAX=7
else #p+p
	SUFFIX="_pp" 
	#PTLEADCUTS=3 #"0 1 3 4 5 6" #"4 5 6"
	PTLEAD_MIN=3
	PTLEAD_MAX=5
	USE2DHISTO=0 #for combined MB+HT spectrum we have only 1D histograms
	TRG="MBHT"
fi
export USE2DHISTO
export PTLEAD_MIN #start with this pTlead cut
export PTLEAD_MAX #finish with this pTlead cut
#export PTLEADCUTS
WRKDIR="$BASEDIR/out/$TRG"

if [ $EFFICORR -eq 0 ]; then
EFFSUF=""
else
EFFSUF="_eff"
fi

if [ $SVD -eq 0 ]; then
UTYPE="Bayes"
else
UTYPE="SVD"
fi

#loop over datasets (for systematic studies)
for SYSSUF in "_main" "_nfit12" "_nfit20" "_RRho02" "_RRho04" "_nrem-1" "_pythia" 
do
	if [ $SYSSUF == "_global" ]; then
		SYSSUF2="_global"
	else
		SYSSUF2=""
	fi

	if [ $SYSSUF == "_main" ]; then
   	TSUFFIX_ARR="_normal _pp _g _u _m5 _p5 _v2"
	else
   	TSUFFIX_ARR="_normal"
	fi

#loop over embedding sets (for systematic studies)
for TSUFF in `echo $TSUFFIX_ARR`
do
export TSUFF

for RPARAM in 0.2 0.3 0.4 #0.5
do
export RPARAM
export DATA_PATH="$WRKDIR/inclusive${SUFFIX}${SYSSUF}"
export PRIOR_PATH="$WRKDIR/prior"
export RMATRIX_PATH="$WRKDIR/embedding${SUFFIX}${SYSSUF}/rmatrix${TSUFF}"
export EPSILON_PATH="$TOYMODELDIR/DataOut/pythia/jetonly/pyEmb_R${RPARAM}${SUFFIX}${SYSSUF2}${TSUFF}/"

#choice of bining arrays 0: nu=nm, 1: nu<nm, 2: nu<nm
for BININGCH in 0 1 4 #0 1 #2 3 4 #1 4 
do
export BININGCH 

OUT_DIR="${DATA_PATH}/Unfolded_R${RPARAM}_${UTYPE}_${NBINS}bins_bining${BININGCH}_${RMATRIX_TYPE}${SUF}${TSUFF}" #/${prior_type[$PRIOR]}"
echo "creating directory: $OUT_DIR"
#we will create a subdirectory for each prior
for PRIOR in 2 4 5 6 7 8 9 10 11 12 13 14 15 #0: truth, 1: flat, 2: biased pythia, 3: pT^(-3), 4:pT^(-4) 5: pT^(-5) 6:pT^(-6) 7: levy I 8: levy II
do
	mkdir -p $OUT_DIR/${prior_type[$PRIOR]}
done
export OUT_DIR

#Prepare job submission
if [ ! -e tmp ]; then
	mkdir -p tmp
fi

TEMPLATE_NAME="unfolding${UTYPE}_cent${CENTRAL}_${RMATRIX_TYPE}_R${RPARAM}_binning${BININGCH}_${SYSSUF}_${TSUFF}.xml"
NFILES=1 #how many files merge into one batch
FILES_PER_HOUR="5.0"

#===========================
#create submission xml file
#===========================
echo "<?xml version=\"1.0\" encoding=\"utf-8\" ?>" > tmp/$TEMPLATE_NAME
echo "<job maxFilesPerProcess=\"$NFILES\" simulateSubmission = \"false\" filesPerHour = \"$FILES_PER_HOUR\">" >> tmp/$TEMPLATE_NAME
echo "<command>" >> tmp/$TEMPLATE_NAME
echo "setenv RMATRIX_TYPE $RMATRIX_TYPE" >> tmp/$TEMPLATE_NAME
echo "setenv SMOOTH $SMOOTH" >> tmp/$TEMPLATE_NAME
echo "setenv NBINS $NBINS" >> tmp/$TEMPLATE_NAME
echo "setenv NITER $NITER" >> tmp/$TEMPLATE_NAME
echo "setenv PTCUT $PTCUT" >> tmp/$TEMPLATE_NAME
echo "setenv SECONDUNFOLD $SECONDUNFOLD" >> tmp/$TEMPLATE_NAME
echo "setenv INPUTITER $INPUTITER" >> tmp/$TEMPLATE_NAME
echo "setenv EFFICORR $EFFICORR" >> tmp/$TEMPLATE_NAME
echo "setenv SUF $SUF" >> tmp/$TEMPLATE_NAME
echo "setenv SUF $SUF" >> tmp/$TEMPLATE_NAME
#echo "setenv PRIORS $PRIORS" >> tmp/$TEMPLATE_NAME
echo "setenv PRIOR_MIN $PRIOR_MIN" >> tmp/$TEMPLATE_NAME
echo "setenv PRIOR_MAX $PRIOR_MAX" >> tmp/$TEMPLATE_NAME
echo "setenv PRIOR_SKIP1 $PRIOR_SKIP1" >> tmp/$TEMPLATE_NAME
echo "setenv PRIOR_SKIP2 $PRIOR_SKIP2" >> tmp/$TEMPLATE_NAME
echo "setenv PRIOR_SKIP3 $PRIOR_SKIP3" >> tmp/$TEMPLATE_NAME
echo "setenv SVD  $SVD" >> tmp/$TEMPLATE_NAME
echo "setenv CENTRAL $CENTRAL" >> tmp/$TEMPLATE_NAME
echo "setenv USE2DHISTO $USE2DHISTO" >> tmp/$TEMPLATE_NAME
#echo "setenv PTLEADCUTS $PTLEADCUTS" >> tmp/$TEMPLATE_NAME
echo "setenv PTLEAD_MIN $PTLEAD_MIN" >> tmp/$TEMPLATE_NAME
echo "setenv PTLEAD_MAX $PTLEAD_MAX" >> tmp/$TEMPLATE_NAME
echo "setenv TSUFF $TSUFF" >> tmp/$TEMPLATE_NAME
echo "setenv RPARAM $RPARAM" >> tmp/$TEMPLATE_NAME
echo "setenv DATA_PATH $DATA_PATH" >> tmp/$TEMPLATE_NAME
echo "setenv PRIOR_PATH $PRIOR_PATH" >> tmp/$TEMPLATE_NAME
echo "setenv RMATRIX_PATH $RMATRIX_PATH" >> tmp/$TEMPLATE_NAME
echo "setenv EPSILON_PATH $EPSILON_PATH" >> tmp/$TEMPLATE_NAME
echo "setenv OUT_DIR $OUT_DIR" >> tmp/$TEMPLATE_NAME
echo "setenv BININGCH  $BININGCH" >> tmp/$TEMPLATE_NAME
echo "  source $ANALYSISDIR/set_paths.csh" >> tmp/$TEMPLATE_NAME
echo "  starver $STARLIB_VER" >> tmp/$TEMPLATE_NAME
echo "  cd $MACRODIR" >> tmp/$TEMPLATE_NAME
echo "  pwd" >> tmp/$TEMPLATE_NAME
echo "  root4star -l -q -b $MACRONAME" >> tmp/$TEMPLATE_NAME
echo "  </command>" >> tmp/$TEMPLATE_NAME
echo "  <stdout URL=\"file:$LOGDIR/\$JOBID.log\"/>" >> tmp/$TEMPLATE_NAME
echo "  <stderr URL=\"file:$ERRDIR/\$JOBID.err\"/>" >> tmp/$TEMPLATE_NAME
echo "  <SandBox>" >> tmp/$TEMPLATE_NAME
echo "  <Package>" >> tmp/$TEMPLATE_NAME
echo "  <File>file:$MACRODIR/$MACRONAME</File>" >> tmp/$TEMPLATE_NAME
echo "  </Package>" >> tmp/$TEMPLATE_NAME
echo "  </SandBox>" >> tmp/$TEMPLATE_NAME
echo "</job>" >> tmp/$TEMPLATE_NAME

#let's submit
	cd tmp
	star-submit $TEMPLATE_NAME 
	cd ..

	#done #prior
	#done #pTleading
done #bining
done #R
done #suffix - embedding set
done #suffix - data set
done #centrality
done #unfolding
