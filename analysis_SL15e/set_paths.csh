#!/bin/csh
set STARLIB_VER = SL15e_embed
set BASEPATH = /star/u/rusnak/JET_analysis_run11/STARJet
setenv STARJETBASEDIR $BASEPATH 
setenv ANALYSISDIR $BASEPATH/analysis_$STARLIB_VER
setenv LD_LIBRARY_PATH $LD_LIBRARY_PATH\:$ANALYSISDIR/lib

setenv ROOUNFOLD $BASEPATH/software_$STARLIB_VER/RooUnfold/v-trunk-custom
setenv LD_LIBRARY_PATH $LD_LIBRARY_PATH\:$ROOUNFOLD

setenv FASTJETDIR $BASEPATH/software_$STARLIB_VER/fastjet3
setenv PATH $PATH\:$FASTJETDIR/bin
setenv LD_LIBRARY_PATH $LD_LIBRARY_PATH\:$FASTJETDIR/lib

setenv TOYMODELDIR /star/u/rusnak/JET_analysis_run11/toymodel
