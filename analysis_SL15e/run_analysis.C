#include <TSystem>

class StMaker;
class StChain;
class StPicoDstMaker;
class StJetAna;

enum picoClass {HF,Alex};

StChain *chain;
void run_analysis(Int_t nEvents = 1000000000, picoClass picoType=HF)
{
//Load all the System libraries
	
   gROOT->LoadMacro("$STAR/StRoot/StMuDSTMaker/COMMON/macros/loadSharedLibraries.C");
	loadSharedLibraries();


	gSystem->Load("$FASTJETDIR/lib/libfastjet.so");
	gSystem->Load("$FASTJETDIR/lib/libfastjettools.so");


	//PYTHIA6 libraries
	gSystem->Load("libEG.so");
	gSystem->Load("libEGPythia6.so");
	gSystem->Load("libPythia6.so");
/*
	gSystem->Load("$PYTHIA6/libPythia6.so");
	gSystem->Load("$PICOREADDIR/libStPicoDstMaker.so");
	gSystem->Load("$ANALYSISDIR/libSJetRun.so");
	gSystem->Load("$ROOTSYS/lib/libEGPythia6.so");
	gSystem->Load("$UNFOLDINGDIR/libUnfold.so");
	gSystem->Load("$ROOUNFOLD/libRooUnfold.so");
*/
	gSystem->Load("$ANALYSISDIR/lib/StPicoDstMaker");
   gSystem->Load("$ANALYSISDIR/lib/StRefMultCorr");
   gSystem->Load("$ANALYSISDIR/lib/StJetTrackEvent");
   gSystem->Load("$ANALYSISDIR/lib/StJetAna");

	if(picoType==HF)
	{
		TString inputFile=gSystem->Getenv("FILELIST");
	
		chain = new StChain();
	
		StPicoDstMaker *picoMaker = new StPicoDstMaker(0,inputFile,"picoDst");
		picoMaker->SetDebug(-3);

		chain->Init();
		cout<<"chain->Init();"<<endl;
	
      //StMyAnalysisMaker *anaMaker = new StMyAnalysisMaker("ana",picoMaker,outputFile);
	   StJetAna *anaMaker = new StJetAna("ana",picoMaker);
		anaMaker->SetDebug(-3);
		anaMaker->Init();

		int total = picoMaker->chain()->GetEntries();
      cout << " Total entries = " << total << endl;
      if(nEvents>total) nEvents = total;
		for (Int_t i=0; i<nEvents; i++){
	  		if(i%1000==0)
				cout << "Working on eventNumber " << i << endl;
			
	  		chain->Clear();
			int iret = chain->Make(i);
		
			if (iret) { cout << "Bad return code!" << iret << endl; break;}

		  	//total++;
		
			anaMaker->Make_HFpico();
	  		if(i%1000==0)
				cout << "anaMaker->Make_HFpico()" << i << endl;

		}
	
		//anaMaker->Finish();
		cout << "****************************************** " << endl;
		cout << "Work done... now its time to close up shop!"<< endl;
		cout << "****************************************** " << endl;
		chain->Finish();
		cout << "****************************************** " << endl;
		cout << "total number of events  " << nEvents << endl;
		cout << "****************************************** " << endl;
		
		delete chain;
	}
	else if(picoType==Alex)
	{
		StJetAna *anaMaker = new StJetAna();
		anaMaker->Init();
		anaMaker->Make_Alexpico(nEvents);
		//anaMaker->Finish();
	}
}
