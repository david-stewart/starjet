#ifndef StJetPythia_h
#define StJetPythia_h 

#include "TString.h"
#include "TH1D.h"
#include "TH2D.h"
#include "TFile.h"
#include "TSystem.h"
#include "TMath.h"

//FastJet 3
#include "fastjet/config.h"
#include "fastjet/PseudoJet.hh"
#include "fastjet/JetDefinition.hh"
#include "fastjet/ClusterSequence.hh"
#include "fastjet/ClusterSequenceArea.hh"
#include "fastjet/Selector.hh"
#include "fastjet/tools/Subtractor.hh"

using namespace std;
using namespace fastjet;

class StJetPythia
{
public:
	StJetPythia();
	virtual ~StJetPythia();
	void Run(int nevt=1,float xsection=1);


	ClassDef(StJetPythia,1)
};
#endif
