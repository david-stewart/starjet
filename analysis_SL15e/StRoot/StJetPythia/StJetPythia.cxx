#include "StJetPythia.h"

//using namespace std;
//using namespace fastjet;

ClassImp(StJetPythia)

//=============================================================================
StJetPythia::StJetPythia()
{
}
//=============================================================================
StJetPythia::~StJetPythia()
{
}
//=============================================================================

void StJetPythia::Run(int nevt,float xsec){
  //load parameters----------------------------------
  TString inDir = gSystem->Getenv("INPUTDIR");
  TString outDir = gSystem->Getenv("OUTPUTDIR");
  int charged = atoi(gSystem->Getenv("CHARGED"));
  float maxrap = atof(gSystem->Getenv("MAXRAP"));
  float r = atof(gSystem->Getenv("RPARAM"));
  float acut = atof(gSystem->Getenv("ACUT"));
  float fpTcut=0.2;

	bool print_jet_info=0;
	bool doJetReco=1; //run jet reconstruction
  int print_parton_energy=0;
	if(charged<=-2)print_parton_energy=1;
  charged=abs(charged);
  //-------------------------------------------------
  //INPUT
  TString infile = "null";
  infile = Form("%s/pythia.dat", inDir.Data());
  cout<<"[i] Input file: "<<infile<<endl;
  //OUTPUT
  TString outfile = "null";
  outfile=Form("%s/starjet_pythia_R%.1lf_charged%d.root",outDir.Data(),r,charged);
  cout<<"[i] Output file: "<<outfile<<endl;
  TFile *foutput=new TFile(outfile.Data(),"RECREATE");

  //control histograms
  TH2D* hNconstPtl0=new TH2D("hNconstPtl0","# of jet const. vs jet pT, p_{T}^{lead}>0; n constituents; p_{T} GeV",20,0.5,20.5,100,0,50); 
  TH2D* hNconstPtl5=new TH2D("hNconstPtl5","# of jet const. vs jet pT, p_{T}^{lead}>5; n constituents; p_{T} GeV",20,0.5,20.5,100,0,50); 
  TH2D* hpTlFracPtl0=new TH2D("hpTlFracPtl0","p_{T}^{lead}/p_{T}^{jet} vs p_{T}^{jet}, p_{T}^{lead}>0; p_{T}^{lead}/p_{T}^{jet}; p_{T} GeV",20,0.1,1.1,100,0,50);
  TH2D* hpTlFracPtl5=new TH2D("hpTlFracPtl5","p_{T}^{lead}/p_{T}^{jet} vs p_{T}^{jet}, p_{T}^{lead}>5; p_{T}^{lead}/p_{T}^{jet}; p_{T} GeV",20,0.1,1.1,100,0,50);

  //histograms
  //int nptbins=1000;//fqw
  int nptbins=2000;float ptminbin=0,ptmaxbin=100;
  int netabins=100*2*maxrap,nphibins=120;
  const int npTlead=6;
  float pTleadCuts[npTlead]={0,3,4,5,6,7};
  TH1I *hevts=new TH1I("hevts","hevts",2,0,2);
  TH1D *hxsec=new TH1D("hxsec","hxsec",1,0,1);
  TH1D *hpt_particle=new TH1D("hpt_particle","particle pT spectrum; p_{T} (GeV/c); 1/(2#pi p_{T}) d^{2}#sigma/(dp_{T}d#eta)",nptbins,ptminbin,ptmaxbin);hpt_particle->Sumw2();
  TH1D *hpt_pi0=new TH1D("hpt_pi0","pi0 pT spectrum; p_{T} (GeV/c); 1/(2#pi p_{T}) d^{2}#sigma/(dp_{T}d#eta)",nptbins,ptminbin,ptmaxbin);hpt_particle->Sumw2();
  TH1D *heta_particle=new TH1D("heta_particle","particle eta",1000,-10,10);heta_particle->Sumw2();
  TH1D *hy_particle=new TH1D("hy_particle","particle rapidity",1000,-10,10);hy_particle->Sumw2();
  //TH2D *hyeta=new TH2D("hyeta","",2000,-1,1,2000,-1,1);
  //TH2D *hmp=new TH2D("hmp","",2000,0,2,1000,0,100);
  TH1D *hpdg_particle=new TH1D("hpdg_particle","particle pdg",10000,-5000,5000);hpdg_particle->Sumw2();
  TH1D *hpT_parton=new TH1D("hpT_parton","hard scattering outgoing parton pT",nptbins,ptminbin,ptmaxbin);hpT_parton->Sumw2();
  TH1D *hpT_pTl[npTlead];
  for(int k=0;k<npTlead;k++){
    TString hname=Form("hpT_pTl%.0lf",pTleadCuts[k]);
    TString hdesc=Form("jet pT for pTlead>%.0lf ; p_{T} [GeV/c]",pTleadCuts[k]);
    hpT_pTl[k]=new TH1D(hname,hdesc,nptbins,ptminbin,ptmaxbin);
    hpT_pTl[k]->Sumw2();
  }
  TH2D *heta_phi=new TH2D("heta_phi","jet eta vs phi;#eta;#phi",netabins,-maxrap,maxrap,nphibins,0,2*TMath::Pi());

  //==================================================================
  //Double32_t rho = 0;     //[0, 0, 16]
  //Double32_t sigma = 0;   //[0, 0, 16]
  //--------------------------------------------------------------
  //StMemStat memstat;
  //memstat.Start();
  //cout<<"Memory used: "<<memstat.Used()<<endl;

  int nevt_read=0;
  int ievt,npart;
  int istat[4000],ipdg[4000],iprnt[4000],idaug1[4000],idaug2[4000],ichge[4000];
  double px[4000],py[4000],pz[4000],energy[4000],mass[4000];
  char name[16];int jj;
  FILE *fp=fopen(infile,"read");
  if(nevt<=0)nevt=1E9;
  int jptot[10000];//fqwang 
  for(int i=0;i<nevt;i++){
    if(fscanf(fp,"%d %d",&ievt,&npart)!=2)break;
    if(i%1000==0)cout<<i<<": event# "<<ievt<<endl;
    for(int j=0;j<npart;j++){//read in the entire event first
      if(fscanf(fp,"%d %d %d %d %d %d %s %d %lf %lf %lf %lf %lf",&jj,istat+j,ipdg+j,iprnt+j,idaug1+j,idaug2+j,name,ichge+j,px+j,py+j,pz+j,energy+j,mass+j)!=13)cout<<"fqwang Error!"<<endl;
    }
    //input to jet reconstruction
    vector<PseudoJet> input_vector;
    for(int j=0;j<npart;j++){
      //cout<<ipdg<<" "<<px<<" "<<py<<" "<<pz<<" "<<energy<<endl;
      if(charged<2&&istat[j]!=1)continue;
      if(charged==1&&!ichge[j])continue;
      if(charged==6){//first-generation shower partons from initial radiation and hard-scattering outgoing partons (no UE partons)
	if(j<8)continue;
if(istat[j]==51)continue;
if(iprnt[j]>8)continue;
if(iprnt[j]>=0&&iprnt[j]<=2)continue;
}else if(charged==7){//first-generation shower partons from hard-scattering outgoing partons
if(j<8)continue;
	if(istat[j]==51)continue;
	if(iprnt[j]!=7&&iprnt[j]!=8)continue;
      }else if(charged>=2){
	if(abs(ipdg[j])>=10&&ipdg[j]!=21&&ipdg[j]!=92)continue;//not quark, gluon, or string
	//if(istat!=11&&istat!=12)continue;
	//if(istat[j]==1)continue;//stable particles//unnecessary to have this statement
	if(istat[j]==21)continue;
	if(istat[j]>20)printf("fqwang-parton-20 %d %d %d %d %d %d %d %d %s %d %lf %lf %lf %lf %lf\n",i+1,npart,j+1,istat[j],ipdg[j],iprnt[j],idaug1[j],idaug2[j],name,ichge[j],px[j],py[j],pz[j],energy[j],mass[j]);
	if(istat[j]>20)continue;
	if(!idaug1[j]&&!idaug2[j])printf("fqwang-parton-0-0 %d %d %d %d %d %d %d %d %s %d %lf %lf %lf %lf %lf\n",i+1,npart,j+1,istat[j],ipdg[j],iprnt[j],idaug1[j],idaug2[j],name,ichge[j],px[j],py[j],pz[j],energy[j],mass[j]);
	if(!idaug1[j]&&!idaug2[j])continue;//final stable particle, not parton//or hard scattering outgoing partons
	//int keep=0;
	int nparton=0;
	for(int k=idaug1[j]-1;k<idaug2[j];++k){
	  //if(istat[k]==1&&!idaug1[k]&&!idaug2[k])keep=1;//at least one daughter is a stable particle
	  if(abs(ipdg[k])<10||ipdg[k]==21||ipdg[k]==92)nparton++;
	}
	if(nparton&&nparton!=idaug2[j]-idaug1[j]+1)printf("fqwang-parton %d %d %d %d %d %d %d %d\n",i+1,npart,j+1,ipdg[j],istat[j],idaug1[j],idaug2[j],nparton);
	if(ipdg[j]==92&&nparton)printf("fqwang-parton-92 %d %d %d %d %d %d %d %d\n",i+1,npart,j+1,ipdg[j],istat[j],idaug1[j],idaug2[j],nparton);
	if(nparton)continue;//has daughter partons so not final state parton
	//if(!keep)continue;
	if(charged==2&&iprnt[j]>=0&&iprnt[j]<=2)continue;//exclude UE partons
	if(charged==3){//trace to 7/8.
	  int k=iprnt[j];
	  //printf("%d: %d ",j,k);
	  //while(k>8){k=iprnt[k-1];printf("%d ",k);}printf("\n");
	  while(k>8)k=iprnt[k-1];
	  if(k!=7&&k!=8)continue;//not from hard scattering
	}
	if(charged==5){//reject if trace to 0/1/2
	  int k=iprnt[j];
	  //printf("%d: %d ",j,k);
	  //while(k>8){k=iprnt[k-1];printf("%d ",k);}printf("\n");
	  while(k>8)k=iprnt[k-1];
	  if(k==0||k==1||k==2)continue;//not from hard scattering
	}
	//if(istat[j]==51)continue;
      }

      double pt2=px[j]*px[j]+py[j]*py[j];
      double pt=sqrt(pt2);
      double p=sqrt(pt2+pz[j]*pz[j]);
      double eta=log((p+pz[j])/pt);
      double rapidity=0.5*log((energy[j]+pz[j])/(energy[j]-pz[j]));
      if(charged==1&&pt<fpTcut)continue;
      //if(fabs(eta)>maxrap+r)continue;//XXXXXX
      if(fabs(eta)>maxrap)continue;
      if(charged==4)continue;
      hpt_particle->Fill(pt,1.0/pt);
      if(ipdg[j]==111)hpt_pi0->Fill(pt,1.0/pt);
      heta_particle->Fill(eta);
      hy_particle->Fill(rapidity);
      //hyeta->Fill(eta,rapidity);
      //hmp->Fill(sqrt(energy[j]*energy[j]-pt2-pz[j]*pz[j]),p);
      hpdg_particle->Fill(ipdg[j]+0.5);
      PseudoJet inp_particle(px[j],py[j],pz[j],energy[j]);
      input_vector.push_back(inp_particle);
    }//end of particel loop

    nevt_read++;
    hevts->Fill(1);
   
   //Fuqiang's strange loop  - not needed
	/*
	for(int j=6;j<8;++j){
      double pt2=px[j]*px[j]+py[j]*py[j];
      double pt=sqrt(pt2);
      double p=sqrt(pt2+pz[j]*pz[j]);
      double eta=log((p+pz[j])/pt);
      if(fabs(eta)>maxrap-r)continue;
      hpT_parton->Fill(pt);
      if(print_parton_energy)printf("%d %d %f %f fqwang_printf %d %f %f %d %f\n",nevt_read,ievt,eta,atan2(py[j],px[j]),-1,pt,pt,1,0.);
      if(charged!=4)continue;//trace from line 7,8. Result as same as 3 above.
      int nptot=1,iptot=0;jptot[0]=j;
      while(iptot<nptot){
	int jj=jptot[iptot];iptot++;
	int nparton=0;
	for(int k=idaug1[jj]-1;k<idaug2[jj];++k){
	  //if(istat[k]==1&&!idaug1[k]&&!idaug2[k])keep=1;//at least one daughter is a stable particle
	  if(abs(ipdg[k])<10||ipdg[k]==21||ipdg[k]==92){
	    jptot[nptot]=k;nptot++;
	    nparton++;
	  }
	}
	if(nparton)continue;
	PseudoJet inp_particle(px[jj],py[jj],pz[jj],energy[jj]);
	pt=sqrt(px[j]*px[j]+py[j]*py[j]);
      hpt_particle->Fill(pt,1.0/pt);
	input_vector.push_back(inp_particle);
      }
    }*/ //end of Fuqiang's strange loop
    if(input_vector.size()<=0)continue;

    //jet reconstruction
    /*
 
    FJWrapper akt_data;
    akt_data.r = r;
    akt_data.maxrap = maxrap;
    akt_data.algor = antikt_algorithm;
    akt_data.input_particles = input_vector;
    akt_data.Run();
*/
	if(!doJetReco)continue;
	//setup fastjet
	float ghost_maxrap=1.0; //max rapidity for ghost jets
	float maxRapJet=1.0-r; //max rapidity for jets
	JetDefinition jet_def(antikt_algorithm, r);
	// jet area definition
	GhostedAreaSpec area_spec(ghost_maxrap);
	//AreaDefinition area_def(active_area, area_spec);
	AreaDefinition area_def(active_area_explicit_ghosts,GhostedAreaSpec(ghost_maxrap,1,0.01));

	//run jet reconstruction
	ClusterSequenceArea clust_seq_hard(input_vector, jet_def, area_def);
	vector<PseudoJet> jets_all = sorted_by_pt(clust_seq_hard.inclusive_jets(fpTcut));
	Selector Fiducial_cut_selector = SelectorAbsEtaMax(maxRapJet); // Fiducial cut for jets
	vector<PseudoJet> jets = Fiducial_cut_selector(jets_all);

    //process jets
	for(int ijet=0; ijet<jets.size(); ijet++){
      double jphi = jets[ijet].phi();
      double jeta = jets[ijet].eta();
      double jpT = jets[ijet].perp();
      //double jM = jets[ijet].m();
      double area = jets[ijet].area(); 

      if(TMath::Abs(jeta) > maxRapJet) continue;
      if(area < acut) continue;

      vector<PseudoJet> constituents = sorted_by_pt(jets[ijet].constituents());
      int Nconst = constituents.size();
      double pTlead=constituents[0].perp();

      //print out jet info --fqwang used to match jets (e.g. chrg vs full) from different jobs
      if(!print_parton_energy && print_jet_info){
	if(charged>=2)printf("%d %d %f %f fqwang_printf %d %f %f %d %f\n",nevt_read,ievt,jeta,jphi,-1,jpT,pTlead,Nconst,area);
	else printf("%d %d %f %f fqwang_printf %d %f %f %d %f\n",nevt_read,ievt,jeta,jphi,1-charged,jpT,pTlead,Nconst,area);
      }

      //fill histos
      hNconstPtl0->Fill(Nconst,jpT);
      hpTlFracPtl0->Fill(pTlead/jpT,jpT);
      if(pTlead>5){
	hNconstPtl5->Fill(Nconst,jpT);
	hpTlFracPtl5->Fill(pTlead/jpT,jpT);
      }

      //---filling histograms--------------
      heta_phi->Fill(jeta,jphi);
      for(int k=0; k<npTlead; k++)if(pTlead>pTleadCuts[k])hpT_pTl[k]->Fill(jpT);
    }//jet loop
  }//end of event loop

  //SCALE HISTOGRAMS
  hxsec->SetBinContent(1,xsec);
  //double jetscale = 1./(2.*TMath::Pi()*2*(maxrap-r)) * xsec / nevt_read;
  double jetscale = 1./(2.*TMath::Pi()*2*(maxrap-r)) * xsec;
  double particle_scale = 1./(2.*TMath::Pi()*2*(maxrap))* xsec;
  for(int k=0; k<npTlead; k++)hpT_pTl[k]->Scale(jetscale, "width");
  hpT_parton->Scale(jetscale,"width");
	hpt_particle->Scale(particle_scale,"width");
	hpt_pi0->Scale(particle_scale,"width");

  foutput->cd();
  //hNconstPtl5->Write();
  //hpTlFracPtl5->Write();
  //hNconstPtl0->Write();
  //hpTlFracPtl0->Write();
  //hevts->Write();
  //hxsec->Write();
  //for(int k=0;k<npTlead;k++)hpT_pTl[k]->Write();
  //heta_phi->Write();
  foutput->Write();
  foutput->Close();
  //memstat.Stop();
}



