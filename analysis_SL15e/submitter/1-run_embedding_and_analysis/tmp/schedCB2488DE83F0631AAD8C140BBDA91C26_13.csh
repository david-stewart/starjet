#!/bin/csh
# -------------------------------------- 
# Script generated on Mon Jul 29 06:00:25 EDT 2019 by the STAR Unified Meta Scheduler 1.10.35 and submitted with:
# cd /star/u/rusnak/JET_analysis_run11/STARJet/analysis_SL15e/submitter/1-run_embedding_and_analysis/tmp; condor_submit -verbose /star/u/rusnak/JET_analysis_run11/STARJet/analysis_SL15e/submitter/1-run_embedding_and_analysis/tmp/schedCB2488DE83F0631AAD8C140BBDA91C26_0_13.condor
# --------------------------------------



if ( $?SUMS_EXECUTION ) then
    goto USERCODESECTION
endif





/bin/echo 'We are starting on node: '`/bin/hostname`


# Preparing environment variables
ENVSETUPSECTION:

setenv FILEBASENAME "no_name"
setenv FILELIST "/star/u/rusnak/JET_analysis_run11/STARJet/analysis_SL15e/submitter/1-run_embedding_and_analysis/tmp/schedCB2488DE83F0631AAD8C140BBDA91C26_13.list"
setenv FILELIST_ALL "/star/u/rusnak/JET_analysis_run11/STARJet/analysis_SL15e/submitter/1-run_embedding_and_analysis/tmp/schedCB2488DE83F0631AAD8C140BBDA91C26.list"
setenv INPUTFILECOUNT "75"
setenv JOBID "CB2488DE83F0631AAD8C140BBDA91C26_13"
setenv JOBINDEX "13"
setenv LOGGING "STD"
setenv REQUESTID "CB2488DE83F0631AAD8C140BBDA91C26"
setenv SUBMITATTEMPT "1"
setenv SUBMITTINGDIRECTORY "/star/u/rusnak/JET_analysis_run11/STARJet/analysis_SL15e/submitter/1-run_embedding_and_analysis/tmp"
setenv SUBMITTINGNODE "rcas6005.rcf.bnl.gov"
setenv SUBMIT_TIME "2019-07-29 10:00:25"
setenv SUMS_AUTHENTICATED_USER "rusnak@rhic.bnl.gov"
setenv SUMS_USER "rusnak"
setenv SUMS_nProcesses "14"
setenv SUMS_name "null"
setenv USEXROOTD "1"
setenv INPUTFILE0 "/star/u/rusnak/pwg/data/128/12128032/st_physics_12128032_raw_1010005.picoDst.root"
setenv INPUTFILE1 "/star/u/rusnak/pwg/data/128/12128032/st_physics_12128032_raw_1010006.picoDst.root"
setenv INPUTFILE2 "/star/u/rusnak/pwg/data/128/12128032/st_physics_12128032_raw_1040001.picoDst.root"
setenv INPUTFILE3 "/star/u/rusnak/pwg/data/128/12128032/st_physics_12128032_raw_1040002.picoDst.root"
setenv INPUTFILE4 "/star/u/rusnak/pwg/data/128/12128032/st_physics_12128032_raw_1040003.picoDst.root"
setenv INPUTFILE5 "/star/u/rusnak/pwg/data/128/12128032/st_physics_12128032_raw_1040004.picoDst.root"
setenv INPUTFILE6 "/star/u/rusnak/pwg/data/128/12128032/st_physics_12128032_raw_2010001.picoDst.root"
setenv INPUTFILE7 "/star/u/rusnak/pwg/data/128/12128032/st_physics_12128032_raw_2010002.picoDst.root"
setenv INPUTFILE8 "/star/u/rusnak/pwg/data/128/12128032/st_physics_12128032_raw_2020001.picoDst.root"
setenv INPUTFILE9 "/star/u/rusnak/pwg/data/128/12128032/st_physics_12128032_raw_2020002.picoDst.root"
setenv INPUTFILE10 "/star/u/rusnak/pwg/data/128/12128032/st_physics_12128032_raw_2020003.picoDst.root"
setenv INPUTFILE11 "/star/u/rusnak/pwg/data/128/12128032/st_physics_12128032_raw_2030001.picoDst.root"
setenv INPUTFILE12 "/star/u/rusnak/pwg/data/128/12128032/st_physics_12128032_raw_2030002.picoDst.root"
setenv INPUTFILE13 "/star/u/rusnak/pwg/data/128/12128032/st_physics_12128032_raw_2040001.picoDst.root"
setenv INPUTFILE14 "/star/u/rusnak/pwg/data/128/12128032/st_physics_12128032_raw_2040002.picoDst.root"
setenv INPUTFILE15 "/star/u/rusnak/pwg/data/128/12128032/st_physics_12128032_raw_2040003.picoDst.root"
setenv INPUTFILE16 "/star/u/rusnak/pwg/data/128/12128032/st_physics_12128032_raw_3010001.picoDst.root"
setenv INPUTFILE17 "/star/u/rusnak/pwg/data/128/12128032/st_physics_12128032_raw_3010002.picoDst.root"
setenv INPUTFILE18 "/star/u/rusnak/pwg/data/128/12128032/st_physics_12128032_raw_3010003.picoDst.root"
setenv INPUTFILE19 "/star/u/rusnak/pwg/data/128/12128032/st_physics_12128032_raw_3010004.picoDst.root"
setenv INPUTFILE20 "/star/u/rusnak/pwg/data/128/12128032/st_physics_12128032_raw_3010005.picoDst.root"
setenv INPUTFILE21 "/star/u/rusnak/pwg/data/128/12128032/st_physics_12128032_raw_3010006.picoDst.root"
setenv INPUTFILE22 "/star/u/rusnak/pwg/data/128/12128032/st_physics_12128032_raw_3020001.picoDst.root"
setenv INPUTFILE23 "/star/u/rusnak/pwg/data/128/12128032/st_physics_12128032_raw_3020002.picoDst.root"
setenv INPUTFILE24 "/star/u/rusnak/pwg/data/128/12128032/st_physics_12128032_raw_3020003.picoDst.root"
setenv INPUTFILE25 "/star/u/rusnak/pwg/data/128/12128032/st_physics_12128032_raw_4010001.picoDst.root"
setenv INPUTFILE26 "/star/u/rusnak/pwg/data/128/12128032/st_physics_12128032_raw_4010002.picoDst.root"
setenv INPUTFILE27 "/star/u/rusnak/pwg/data/128/12128032/st_physics_12128032_raw_4010003.picoDst.root"
setenv INPUTFILE28 "/star/u/rusnak/pwg/data/128/12128032/st_physics_12128032_raw_4010004.picoDst.root"
setenv INPUTFILE29 "/star/u/rusnak/pwg/data/128/12128032/st_physics_12128032_raw_4010005.picoDst.root"
setenv INPUTFILE30 "/star/u/rusnak/pwg/data/128/12128032/st_physics_12128032_raw_4010006.picoDst.root"
setenv INPUTFILE31 "/star/u/rusnak/pwg/data/128/12128032/st_physics_12128032_raw_4020001.picoDst.root"
setenv INPUTFILE32 "/star/u/rusnak/pwg/data/128/12128032/st_physics_12128032_raw_4020002.picoDst.root"
setenv INPUTFILE33 "/star/u/rusnak/pwg/data/128/12128032/st_physics_12128032_raw_4020003.picoDst.root"
setenv INPUTFILE34 "/star/u/rusnak/pwg/data/128/12128032/st_physics_12128032_raw_5010001.picoDst.root"
setenv INPUTFILE35 "/star/u/rusnak/pwg/data/128/12128032/st_physics_12128032_raw_5010002.picoDst.root"
setenv INPUTFILE36 "/star/u/rusnak/pwg/data/128/12128032/st_physics_12128032_raw_5010003.picoDst.root"
setenv INPUTFILE37 "/star/u/rusnak/pwg/data/128/12128032/st_physics_12128032_raw_5010004.picoDst.root"
setenv INPUTFILE38 "/star/u/rusnak/pwg/data/128/12128032/st_physics_12128032_raw_5010005.picoDst.root"
setenv INPUTFILE39 "/star/u/rusnak/pwg/data/128/12128032/st_physics_12128032_raw_5020001.picoDst.root"
setenv INPUTFILE40 "/star/u/rusnak/pwg/data/128/12128032/st_physics_12128032_raw_5030001.picoDst.root"
setenv INPUTFILE41 "/star/u/rusnak/pwg/data/128/12128032/st_physics_12128032_raw_5040001.picoDst.root"
setenv INPUTFILE42 "/star/u/rusnak/pwg/data/128/12128032/st_physics_12128032_raw_5040002.picoDst.root"
setenv INPUTFILE43 "/star/u/rusnak/pwg/data/128/12128032/st_physics_12128032_raw_5040003.picoDst.root"
setenv INPUTFILE44 "/star/u/rusnak/pwg/data/128/12128032/st_physics_adc_12128032_raw_0520001.picoDst.root"
setenv INPUTFILE45 "/star/u/rusnak/pwg/data/128/12128032/st_physics_adc_12128032_raw_1510001.picoDst.root"
setenv INPUTFILE46 "/star/u/rusnak/pwg/data/128/12128032/st_physics_adc_12128032_raw_1510002.picoDst.root"
setenv INPUTFILE47 "/star/u/rusnak/pwg/data/128/12128032/st_physics_adc_12128032_raw_1510003.picoDst.root"
setenv INPUTFILE48 "/star/u/rusnak/pwg/data/128/12128032/st_physics_adc_12128032_raw_2490001.picoDst.root"
setenv INPUTFILE49 "/star/u/rusnak/pwg/data/128/12128032/st_physics_adc_12128032_raw_2490002.picoDst.root"
setenv INPUTFILE50 "/star/u/rusnak/pwg/data/128/12128032/st_physics_adc_12128032_raw_2520001.picoDst.root"
setenv INPUTFILE51 "/star/u/rusnak/pwg/data/128/12128032/st_physics_adc_12128032_raw_3500001.picoDst.root"
setenv INPUTFILE52 "/star/u/rusnak/pwg/data/128/12128032/st_physics_adc_12128032_raw_3500002.picoDst.root"
setenv INPUTFILE53 "/star/u/rusnak/pwg/data/128/12128032/st_physics_adc_12128032_raw_3500003.picoDst.root"
setenv INPUTFILE54 "/star/u/rusnak/pwg/data/128/12128032/st_physics_adc_12128032_raw_4500001.picoDst.root"
setenv INPUTFILE55 "/star/u/rusnak/pwg/data/128/12128032/st_physics_adc_12128032_raw_4500002.picoDst.root"
setenv INPUTFILE56 "/star/u/rusnak/pwg/data/128/12128032/st_physics_adc_12128032_raw_4500003.picoDst.root"
setenv INPUTFILE57 "/star/u/rusnak/pwg/data/128/12128032/st_physics_adc_12128032_raw_5520001.picoDst.root"
setenv INPUTFILE58 "/star/u/rusnak/pwg/data/128/12128032/st_physics_adc_12128032_raw_5520002.picoDst.root"
setenv INPUTFILE59 "/star/u/rusnak/pwg/data/128/12128038/st_physics_12128038_raw_0010001.picoDst.root"
setenv INPUTFILE60 "/star/u/rusnak/pwg/data/128/12128038/st_physics_12128038_raw_1010001.picoDst.root"
setenv INPUTFILE61 "/star/u/rusnak/pwg/data/128/12128038/st_physics_12128038_raw_1030001.picoDst.root"
setenv INPUTFILE62 "/star/u/rusnak/pwg/data/128/12128038/st_physics_12128038_raw_1040001.picoDst.root"
setenv INPUTFILE63 "/star/u/rusnak/pwg/data/128/12128038/st_physics_12128038_raw_2010001.picoDst.root"
setenv INPUTFILE64 "/star/u/rusnak/pwg/data/128/12128038/st_physics_12128038_raw_2020001.picoDst.root"
setenv INPUTFILE65 "/star/u/rusnak/pwg/data/128/12128038/st_physics_12128038_raw_3010001.picoDst.root"
setenv INPUTFILE66 "/star/u/rusnak/pwg/data/128/12128038/st_physics_12128038_raw_4010001.picoDst.root"
setenv INPUTFILE67 "/star/u/rusnak/pwg/data/128/12128038/st_physics_12128038_raw_4020001.picoDst.root"
setenv INPUTFILE68 "/star/u/rusnak/pwg/data/128/12128038/st_physics_12128038_raw_5020001.picoDst.root"
setenv INPUTFILE69 "/star/u/rusnak/pwg/data/128/12128038/st_physics_12128038_raw_5030001.picoDst.root"
setenv INPUTFILE70 "/star/u/rusnak/pwg/data/128/12128038/st_physics_adc_12128038_raw_1500001.picoDst.root"
setenv INPUTFILE71 "/star/u/rusnak/pwg/data/128/12128038/st_physics_adc_12128038_raw_2510001.picoDst.root"
setenv INPUTFILE72 "/star/u/rusnak/pwg/data/128/12128038/st_physics_adc_12128038_raw_3490001.picoDst.root"
setenv INPUTFILE73 "/star/u/rusnak/pwg/data/128/12128038/st_physics_adc_12128038_raw_4490001.picoDst.root"
setenv INPUTFILE74 "/star/u/rusnak/pwg/data/128/12128038/st_physics_adc_12128038_raw_5500001.picoDst.root"

# This block is used by CONDOR where $HOME in not set

if ( ! $?USER ) then
    echo "USER is not defined"
    set USER=`id | sed "s/).*//" | sed "s/.*(//"`
endif
if ( ! $?HOME ) then
    echo "HOME is not defined"

    if ( -x /usr/bin/getent ) then
	# we have getent, should not be on aix, bsd, Tru64 however
	# will work for Linux
	echo "Using getent method"
	setenv HOME `/usr/bin/getent passwd $USER | /bin/sed 's|.*\:.*\:.*\:.*\:\([^\:]*\):.*|\1|'`
    else 
	set PTEST=`which perl`
	if ( "$PTEST" != "" ) then
	    echo "Using perl method"
	    # we have perl defined, we can get info from there
	    /bin/cat <<EOF >test$$.pl
my(\$user) = getpwuid(\$<);
@items = getpwnam(\$user);
print \$items[7];
EOF
	    setenv HOME `$PTEST test$$.pl` && /bin/rm  -f test$$.pl
	else
	    set CTEST=`which cc`
	    if ( "$CTEST" != "" ) then
		echo "Using C code method"
		# use C code for doing this
		/bin/cat <<EOF >test$$.c
#include <stdio.h>
#include <unistd.h>
#include <pwd.h>


int main()
{
  struct passwd *info;
  uid_t          uid;

  /* get process UID */
  uid = getuid();
  info= getpwuid(uid);

  (void) printf("%s\n",info->pw_dir);
  return 0;
} 

EOF
		$CTEST -o test$$ test$$.c
		/bin/chmod +x test$$
		setenv HOME `./test$$` && /bin/rm -f test$$ test$$.c
	    else
		echo "We have no ways to define HOME and it is not defined"
		exit
	    endif
	endif
    endif
endif

echo "HOME is now $HOME"


# Default value for path if not defined.
if ( ! $?PATH ) then
   setenv PATH /usr/local/bin:/bin:/usr/bin
endif


/usr/bin/test -r $HOME/.cshrc && source $HOME/.cshrc

# Creating the scratch directory, return failure status
set SUMS_try=0
set SUMS_PAD=""

MKDTRY:
setenv SCRATCH "/tmp/$USER$SUMS_PAD/$JOBID"
/bin/mkdir -p $SCRATCH   >& /dev/null
set STS=$status
if (! -d $SCRATCH) then
       #test if porper UID
       set SUMS_IsUID=`(/usr/bin/test -O /tmp/$USER$SUMS_PAD && echo 1) || echo 0`
       if ( $SUMS_try == 0 &&  -e "/tmp/$USER$SUMS_PAD" && ! $SUMS_IsUID) then
            # First try, directory exists but is not owned by $USER
            # Create a different path and try again
            echo "Scheduler:: $SCRATCH not owned by $USER, trying alternative"
            @ try++
            set SUMS_seed=`/bin/date +%j%H%M%S`
            set SUMS_PAD=`expr $SUMS_seed  \* 25973 \% 100`
            goto MKDTRY
        else
		 echo "Scheduler:: Failed to create $SCRATCH on $HOST"
	         exit $STS
        endif

endif


##########################################################
# Detached script pid so that signals will propagate to  #
# child processes such as root4star, giving a chance to  #
# close out open files.                                  #
##########################################################
if ( ! $?SUMS_EXECUTION ) then
    echo "$$ We will spawn ourselves ($0 $*)"
    echo "$$ We BEGIN on [`/bin/date`]"

    onintr SUMS_SIGHANDLER
    ((setenv SUMS_EXECUTION 1 && nohup $0 $*) > $SCRATCH/.output.log ) >& $SCRATCH/.errror.log & 
    set PID=$!

    echo "$$ We are waiting for $PID "
    set loop=0
    set DELAY=1
    set CHLDPID=""
    set CHKP=10
    while (`/bin/ps --no-headers -p $PID` != "")
       if ( $loop % $CHKP == 0 ) then
           # check this only once every CHKP*DELAY seconds
           set GTEST=`/bin/ps -efH | /bin/grep $PID | /bin/grep -v $$ |  /bin/grep -v grep | /usr/bin/head -1 | /usr/bin/awk '{print $2}'`
           if ( "$GTEST" != "" && "$GTEST" != "$CHLDPID") then
               echo "$$ Grandchild PID is $GTEST - found in $loop x $DELAY seconds"
               if ( "$CHLDPID" == "") then
                   echo "$$ Establishing first watcher   on    `/bin/date`"
                   set DELAY=2
               else
                   echo "$$ Grandchild to watch has changed on `/bin/date`"
               endif
               set CHLDPID="$GTEST"
               echo "$$ Process tree is $$ (main) -> $PID (child) -> $CHLDPID (grandchild)"
               /bin/ps -l $CHLDPID
            endif
       endif
       @ loop++
       sleep $DELAY
   end
   cat $SCRATCH/.output.log >> /dev/stdout
    cat $SCRATCH/.errror.log >> /dev/stderr

# Delete the scratch directory
    /bin/rm -fr $SCRATCH
    echo "$$ Execution of ($0 $*) has ended, exiting normally"
    echo "$$ We END on [`/bin/date`]"
    exit 0

    SUMS_SIGHANDLER:
        echo "$$ We received a signal - sending TERM to PID=$PID group"
        kill -TERM -$PID
        echo "$$ We have sent an TERM signal to $PID and waiting for $CHLDPID on `/bin/date`"
        while (`/bin/ps --no-headers -p $CHLDPID` != "")
           sleep 2
        end
        echo "$$ Dumping the output/error channels ---->"
        cat $SCRATCH/.output.log >> /dev/stdout
        cat $SCRATCH/.errror.log >> /dev/stderr
        echo "$$ <---- done with output/error channels"
        echo "$$ We are now leaving with error on [`/bin/date`]"
        exit 1
endif

USERCODESECTION:
echo "$$ Stepping into the main SUMS wrapper program with [$*]"




#import packages 
echo '.... building sandbox'
setenv pwd `pwd` 
foreach object ( `find /star/u/rusnak/JET_analysis_run11/STARJet/analysis_SL15e_embed/ -maxdepth 1 -name 'run_analysis.C' ` ) 
 
 
          if($object != '/star/u/rusnak/JET_analysis_run11/STARJet/analysis_SL15e_embed/') then 
 
                 foreach DIRObject ( `find $object`) 
 
                        setenv NewLink  $SCRATCH/`echo $DIRObject  | /bin/sed 's|/star/u/rusnak/JET_analysis_run11/STARJet/analysis_SL15e_embed/||'` 
 
                        if(! -e $NewLink) then 
                             if(-f $DIRObject) then 
                                   /bin/ln -s  `echo $DIRObject | /bin/sed "s|\(^[^/]\)\(.*\)|$pwd/\1\2|"`  $NewLink 
                             else 
                                   /bin/mkdir -p   $NewLink 
                             endif 
                        endif 
               end 
 
 
        endif 
 
end 


# Used for cleanning up the sandbox later
/bin/ls $SCRATCH > ${SCRATCH}/.sandboxFiles
echo ".sandboxFiles" >> ${SCRATCH}/.sandboxFiles



#Note: The default directory in which jobs start has been fix to $SCRATCH
cd $SCRATCH

#--------------- Copy input to SCRATCH --------------------

setenv LocalInputFileDir ${SCRATCH}/INPUTFILES
mkdir ${LocalInputFileDir}

setenv NewFILELIST ${LocalInputFileDir}/${JOBID}.local.list
touch $NewFILELIST

set XRootCopySuccessCount=0
set XRootCopyCount=0
set FileIndex=0
foreach RemoteFile ( `cat ${FILELIST} | awk '{printf($1"\n")}'` )
        set RemoteFileWithoutEvents=${RemoteFile}
        set RemoteFile=`cat ${FILELIST} | grep ${RemoteFile}`
        set nEvents=`echo ${RemoteFile} | awk '{printf($2"\n")}'`
        set isRootFile=`echo ${RemoteFile} | grep -e "^[Rr]oot" | wc -l`

        if(${isRootFile}) then
                  @ XRootCopyCount++
                  set LocalFile=${LocalInputFileDir}/`echo ${RemoteFileWithoutEvents} | sed 's|\(.*/\)\([^\]*\)$|\2|g'`
                  echo running: xrdcp --retry 3 ${RemoteFileWithoutEvents} ${LocalFile} \>\& /dev/null
                  xrdcp --retry 3 ${RemoteFileWithoutEvents} ${LocalFile} >& /dev/null
                  echo "xrdcp copy done: `/bin/date`"
                  if( -e ${LocalFile} ) then
                        @ XRootCopySuccessCount++
                        echo "${LocalFile} ${nEvents}" >> $NewFILELIST
                  else
                        set LocalFile="${RemoteFile}"
                        echo ${LocalFile} >> $NewFILELIST
                  endif
        else
                set LocalFile=${LocalInputFileDir}/`echo ${RemoteFileWithoutEvents} | sed 's|\(.*/\)\([^\]*\)$|\2|g'`
                echo Running: cp ${RemoteFileWithoutEvents}  ${LocalFile}
                cp ${RemoteFileWithoutEvents}  ${LocalFile}
                if( -e ${LocalFile} ) then
                        echo "${LocalFile} ${nEvents}" >> $NewFILELIST
                else
                        echo ${RemoteFile} >> $NewFILELIST
                        set LocalFile="${RemoteFile}"
                endif
        endif

        set LocalFileWithoutEvents=`echo ${LocalFile} | awk '{printf($1"\n")}'`
        setenv INPUTFILE${FileIndex} ${LocalFileWithoutEvents}
        @ FileIndex++
end

echo Swapping \$FILELIST with: ${NewFILELIST}
setenv FILELIST ${NewFILELIST}

if( ${XRootCopyCount} != 0 ) then
        set XRootCopySuccessPercent=`echo "${XRootCopySuccessCount} * 100 / ${XRootCopyCount}" | bc`
        echo "Copied ${XRootCopyCount} files from Xrootd, ${XRootCopySuccessCount} succeeded (${XRootCopySuccessPercent} %)"
endif


echo +++++++++++local copy debug++++++++++++++++++++
echo newFileList: ${FILELIST}
echo ++++++++++++++++cat FILELIST+++++++++++++++++++
cat ${FILELIST}
echo ++++++++++++++++++++find.++++++++++++++++++++++
find .
echo ++++++++++++++++INPUTFILE Var++++++++++++++++++
printenv | grep INPUTFILE
echo +++++++++++++++++++++++++++++++++++++++++++++++

#----------------- End of input copy ----------------------



echo "--------------------------------"
echo 'STAR Unified Meta Scheduler 1.10.35 we are starting in $SCRATCH :' `/bin/pwd`
echo "--------------------------------"
/bin/ls -l

###################################################
# User command BEGIN ----------------------------->

setenv TRIGGER MB
setenv DOAUAU 1
setenv DOQA 0
setenv SAVETREE 0
setenv DOEMBEDDING 0
setenv PYTHIAEMB 0
setenv PARTON g
setenv DOEVENTCUTS 1
setenv GLOBAL 0
setenv RRHO 0.3
setenv ZVERTEX 30
setenv ZTPCZVPD 4.0
setenv TOFBEMC 0
setenv BBCMIN 0
setenv BBCMAX 1000000000
setenv MAXRAP 1.0
setenv DCA 1.0
setenv CHI2 100.0
setenv NFIT 20
setenv NFITNMAX 0.52
setenv ALEXPICO 0
setenv NEVENTS 100000
setenv CENTRAL 1
setenv REFMULTMIN 396
setenv REFMULTMAX 100000
setenv NJETSREMOVE 2
setenv MACRODIR /star/u/rusnak/JET_analysis_run11/STARJet/analysis_SL15e_embed
setenv OUT_PATH /tmp/rusnak
  starver SL15e_embed
  source /star/u/rusnak/JET_analysis_run11/STARJet/analysis_SL15e_embed/set_paths.csh
  cd /star/u/rusnak/JET_analysis_run11/STARJet/analysis_SL15e_embed
  pwd
  root4star -q -b -l run_analysis.C\(100000,0\)
  
# <------------------------------User command END
###################################################

#Sandbox cleanup script, so user will not copy back sandbox files by using *.*
foreach SANDBOXFILE (`/bin/cat ${SCRATCH}/.sandboxFiles`)
   echo "Cleaning up sandbox file:  ${SCRATCH}/${SANDBOXFILE}"
   /bin/rm -fr ${SCRATCH}/${SANDBOXFILE}
end


# Copy output files (if any where specified)
    
    ########### Copy Command Block ########### 
         
    rm -f $SCRATCH/count.$JOBID.test            
    (/bin/ls $SCRATCH/*.root > $SCRATCH/count.$JOBID.test) >& /dev/null
    if ( -z $SCRATCH/count.$JOBID.test ) then               
        echo 'Error: Scheduler could not find any files in $SCRATCH matching your <output> tag fromScratch value: *.root '
    else                      
        echo "Starting copy block"
        foreach file ( $SCRATCH/*.root )
             set i=0

             if ( -d $file ) then
                set cpCommand="/bin/cp -r $file /star/u/rusnak/JET_analysis_run11/STARJet/out/MB//incl_AuAu_0-10cent_HFpico_SL15_evtcts1_rhoR0.3_nrem2_nFitnMax0.52_NFIT20_dca1.0_chi2-100.0_glob0_CodeQA/"
             else
                set cpCommand="/bin/cp -fp $file /star/u/rusnak/JET_analysis_run11/STARJet/out/MB//incl_AuAu_0-10cent_HFpico_SL15_evtcts1_rhoR0.3_nrem2_nFitnMax0.52_NFIT20_dca1.0_chi2-100.0_glob0_CodeQA/"
             endif

             RETRY_CP:
             echo "Running copy command: " $cpCommand
             $cpCommand
             if ( $status && $i < 15 ) then
                 echo "The copy failed, waiting to try again."
                 @ i++
                 sleep 60
                 echo "Trying copy again at "`/bin/date`
                 goto RETRY_CP
             endif
        end
        
    endif
    ##########################################
            
